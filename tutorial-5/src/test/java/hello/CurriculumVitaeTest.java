package hello;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CurriculumVitaeController.class)
public class CurriculumVitaeTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void cvWithNoUser() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("This is my CV")));
    }

    @Test
    public void cvWithUser() throws Exception {
        mockMvc.perform(get("/cv").param("visitor", "Alif"))
                .andExpect(content().string(containsString("Alif, I hope you interested to hire me")));
    }
}
