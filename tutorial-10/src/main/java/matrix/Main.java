package matrix;

import java.io.*;
import java.util.ArrayList;

public class Main {
    private static String genericMatrixPath = "plainTextDirectory/input/matrixProblem";
    private static String pathFileMatrix1 = genericMatrixPath + "B/matrix10rows50columns.txt";
    private static int numberOfLine1 = 50;

    private static String pathFileMatrix2 = genericMatrixPath + "B/matrix50row10column.txt";
    private static int numberOfLine2 = 50;

    private static String pathFileMatrix3 = genericMatrixPath + "A/matrixProblemSet1.txt";
    private static String pathFileMatrix4 = genericMatrixPath + "A/matrixProblemSet2.txt";

    public static void main(String[] args) throws
            IOException, InvalidMatrixSizeForMultiplicationException {

        //Convert into array
        double[][] firstMatrix = convertInputFileToMatrix(pathFileMatrix1, numberOfLine1);
        double[][] secondMatrix = convertInputFileToMatrix(pathFileMatrix2, numberOfLine2);
        double[][] thirdMatrix = convertInputFileToMatrix(pathFileMatrix3,numberOfLine1);
        double[][] fourthMatrix = convertInputFileToMatrix(pathFileMatrix4,numberOfLine2);

        printMatrix(firstMatrix,"plainTextDirectory/input/FirstMatrix.txt");
        printMatrix(secondMatrix,"plainTextDirectory/input/SecondMatrix.txt");

        //TODO Implement, do your benchmark test for these algorithm start from here
        double averageTimeForBasicMultiplication = countAverageTimeBasicMultiplication(thirdMatrix,fourthMatrix,10000,500);
        System.out.println(averageTimeForBasicMultiplication);
        double averageTimeForStrassenMultiplication = countAverageTimeStrassenMultiplication(thirdMatrix,fourthMatrix,10000,500);
        System.out.println(averageTimeForStrassenMultiplication);

        //Example usage of basic multiplication algorithm.
        double[][] multiplicationResult =
                MatrixOperation.basicMultiplicationAlgorithm(firstMatrix, secondMatrix);

        printMatrix(multiplicationResult,"plainTextDirectory/input/MatrixResultFromMultiplication.txt");

        //Example usage of strassen multiplication algorithm.
        //double[][] strassenMultiplicationResult =
               // StrassensAlgorithm.strassenMultiplicationAlgorithm(firstMatrix, secondMatrix);

        //printMatrix(strassenMultiplicationResult,"plainTextDirectory/input/MatrixResultFromStrassen.txt");
    }

    /**
     * Converting a file input into an 2 dimensional array of double that represent a matrix.
     * @param pathFile is a path to file input.
     * @param numberOfLine the number of row (and possibly column) inside the square matrix.
     * @return 2 dimensional array of double representing matrix.
     * @throws IOException in the case of the file is not found because of the wrong path of file.
     */
    public static double[][] convertInputFileToMatrix(String pathFile, int numberOfLine)
            throws IOException {
        File matrixFile = new File(pathFile);
        FileReader fileReader = new FileReader(matrixFile);

        ArrayList<double[]> matrixTemp = new ArrayList<>();
        //double[][] matrix = new double[numberOfLine][numberOfLine];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int sumOfRow = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            //matrix[indexOfLine] = sequenceIntoArray(currentLine);
            double[] row = sequenceIntoArray(currentLine);
            matrixTemp.add(row);
            sumOfRow++;
        }
        double[][] matrix = new double[sumOfRow][matrixTemp.get(0).length];
        for (int i = 0; i < matrix.length; i++){
            matrix[i] = matrixTemp.get(i);
        }
        return matrix;
    }

    /**
     * Converting a row of sequence of double into an array.
     * @param currentLine sequence of double from input representing a row from matrix.
     * @return array of double representing a row from matrix.
     */
    private static double[] sequenceIntoArray(String currentLine) {
        String[] arrInput = currentLine.split(" ");
        double[] arrInteger = new double[arrInput.length];
        for (int index = 0; index < arrInput.length; index++) {
            arrInteger[index] = Double.parseDouble(arrInput[index]);
        }
        return arrInteger;
    }

    private static void printMatrix(double[][] matrix,String path) throws IOException{
        BufferedWriter bw = new BufferedWriter(new FileWriter(path));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[0].length; j++){
                if(j == matrix[0].length-1) sb.append(matrix[i][j]);
                else sb.append(matrix[i][j] + " ");
            }
            sb.append("\n");
        }
        String res = sb.toString();
        bw.write(res);
        bw.close();
    }

    private static double countAverageTimeBasicMultiplication(double[][] firstMatrix, double[][] secondMatrix, double numberOfExperiments, int warmUpIteration)
            throws InvalidMatrixSizeForMultiplicationException{
        for(int i = 0; i < warmUpIteration; i++){
            MatrixOperation.basicMultiplicationAlgorithm(firstMatrix,secondMatrix);
        }
        double sumOfTime = 0;
        for(int i = 0; i < numberOfExperiments; i++){
            long totalMilisForBasicMultiplication = System.currentTimeMillis();
            double[][] multiplicationResult = MatrixOperation.basicMultiplicationAlgorithm(firstMatrix, secondMatrix);
            totalMilisForBasicMultiplication = System.currentTimeMillis() - totalMilisForBasicMultiplication;
            sumOfTime += totalMilisForBasicMultiplication;
        }
        return sumOfTime/numberOfExperiments;
    }

    private static double countAverageTimeStrassenMultiplication(double[][] firstMatrix, double[][] secondMatrix, double numberOfExperiments, int warmUpIteration)
            throws InvalidMatrixSizeForMultiplicationException{
        for(int i = 0; i < warmUpIteration; i++){
            MatrixOperation.basicMultiplicationAlgorithm(firstMatrix,secondMatrix);
        }
        double sumOfTime = 0;
        for(int i = 0; i < numberOfExperiments; i++){
            long totalMilisForStrassenMultiplication = System.currentTimeMillis();
            double[][] multiplicationResult = MatrixOperation.basicMultiplicationAlgorithm(firstMatrix, secondMatrix);
            totalMilisForStrassenMultiplication = System.currentTimeMillis() - totalMilisForStrassenMultiplication;
            sumOfTime += totalMilisForStrassenMultiplication;
        }
        return sumOfTime/numberOfExperiments;
    }


}
