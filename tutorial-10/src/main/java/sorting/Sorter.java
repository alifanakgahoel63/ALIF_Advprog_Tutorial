package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] fasterSort(int[] inputArr,int l, int r) {
        /*for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }*/
        if(l < r){
            int mid = (l+r)/2;
            fasterSort(inputArr,l,mid);
            fasterSort(inputArr,mid+1,r);
            merge(inputArr,l,mid,r);
        }
        return inputArr;
    }

    public static void merge(int arr[],int l, int m, int r){
        int[] b = new int[Main.getNumberOfItemToBeSorted()];
        int i, j, k;
        k = 0;
        i = l;
        j = m+1;
        while (i <= m && j <= r){
            if(arr[i] < arr[j]) b[k++] = arr[i++];
            else b[k++] = arr[j++];
        }
        while (i <= m) b[k++] = arr[i++];
        while (j <= r) b[k++] = arr[j++];
        for (i = r; i >= l; i--){
            arr[i] = b[--k];
        }
    }
}
