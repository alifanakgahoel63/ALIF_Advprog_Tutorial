package sorting;

import java.io.*;

public class Main {

    private static String pathFile = "plainTextDirectory/input/sortingProblem.txt";
    private static int numberOfItemToBeSorted = 50000;

    public static void main(String[] args) throws IOException {

        int[] sequenceInput = convertInputFileToArray();

        //Searching Input Before Sorting
        long totalMilisSearchBeforeSort = System.currentTimeMillis();
        int searchingResultBeforeSort = Finder.slowSearch(sequenceInput,40738);
        totalMilisSearchBeforeSort = System.currentTimeMillis() - totalMilisSearchBeforeSort;
        System.out.println("Searching Complete in " + totalMilisSearchBeforeSort + " milisecond (Before Sorting)");

        //testing apakah output searching before sorting sudah benar
        BufferedWriter bw3 = new BufferedWriter(new FileWriter("plainTextDirectory/input/SearchingResultBeforeSorting.txt"));
        bw3.write(searchingResultBeforeSort + "\n");
        bw3.close();

        //Sorting Input
        long totalMilisSorting = System.currentTimeMillis();
        int[] sortedInput = Sorter.fasterSort(sequenceInput,0,numberOfItemToBeSorted-1);
        totalMilisSorting = System.currentTimeMillis() - totalMilisSorting;
        System.out.println("Sorting Complete in " + totalMilisSorting + " milisecond");

        //testing apakah output sorting sudah benar
        BufferedWriter bw2 = new BufferedWriter(new FileWriter("plainTextDirectory/input/SortingResult.txt"));
        for(int i = 0; i < numberOfItemToBeSorted-1; i++){
            bw2.write(sequenceInput[i] + "\n");
        }
        bw2.close();

        //Searching Input After Sorting
        long totalMilisSearchAfterSort = System.currentTimeMillis();
        int searchingResultAfterSort = Finder.fasterSearch(sequenceInput,0,numberOfItemToBeSorted-1, 40738);
        totalMilisSearchAfterSort = System.currentTimeMillis() - totalMilisSearchAfterSort;
        System.out.println("Searching Complete in " + totalMilisSearchAfterSort + " milisecond (After Sorting)");

        //testing apakah output searching after sorting sudah benar
        BufferedWriter bw = new BufferedWriter(new FileWriter("plainTextDirectory/input/SearchingResultAfterSorting.txt"));
        bw.write(searchingResultAfterSort + "\n");
        bw.close();
    }

    /**
     * Converting a file input into an array of integer.
     * @return an array of integer that represent an integer sequence.
     * @throws IOException in the case of the file is not found because of the wrong path of file.
     */
    private static int[] convertInputFileToArray() throws IOException {
        File sortingProblemFile = new File(pathFile);
        FileReader fileReader = new FileReader(sortingProblemFile);
        int[] sequenceInput = new int[numberOfItemToBeSorted];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int indexOfSequence = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            sequenceInput[indexOfSequence] = Integer.parseInt(currentLine);
            indexOfSequence++;
        }
        return sequenceInput;
    }

    public static int getNumberOfItemToBeSorted(){
        return numberOfItemToBeSorted;
    }
}
