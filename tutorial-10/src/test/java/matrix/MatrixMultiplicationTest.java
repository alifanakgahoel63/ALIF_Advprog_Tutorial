package matrix;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertArrayEquals;
public class MatrixMultiplicationTest {
    //TODO Implement, apply your test cases here
    private static double[][] firstMatrix = {{1,3,5,7},{2,4,6,8}};
    private static double[][] secondMatrix = {{1,8,9},{2,7,10},{3,6,11},{4,5,12}};

    @Test
    public void testMultiplicationAlgorithmIsTrue() throws Exception{
        double[][] actual = MatrixOperation.basicMultiplicationAlgorithm(firstMatrix,secondMatrix);
        double[][] expected = {{50,94,178},{60,120,220}};
        assertArrayEquals(expected,actual);
    }

    @Test
    public void convertInputFileToMatrixTest() throws IOException{
        String path =  "plainTextDirectory/input/MatrixTest.txt";
        double[][] actual = Main.convertInputFileToMatrix(path,50);
        double[][] expected = {{1,2,3},{4,5,6}};
        assertArrayEquals(expected,actual);
    }
}
