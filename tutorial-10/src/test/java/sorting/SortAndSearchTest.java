package sorting;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class SortAndSearchTest {
    //TODO Implement, apply your test cases here
    private static final int[] ARRAY_TEST = {2,7,5,19,14,23};
    private static int[] ARRAY_TEST_SORTED = {2,5,7,14,19,23};

    @Test
    public void testSearchingBeforeSortingIsTrue(){
        int actual = Finder.slowSearch(ARRAY_TEST,19);
        int expected = 19;
        assertEquals(expected,actual);
    }

    @Test
    public void testSortingIsTrue(){
        int[] actual = Sorter.fasterSort(ARRAY_TEST,0,ARRAY_TEST.length-1);
        int[] expected = {2,5,7,14,19,23};
        assertArrayEquals(expected,actual);
    }

    @Test
    public void testSearchingAfterSortingIsTrue(){
        int actual = Finder.fasterSearch(ARRAY_TEST_SORTED,0,ARRAY_TEST_SORTED.length-1,19);
        int expected = 19;
        assertEquals(expected,actual);
    }
}
