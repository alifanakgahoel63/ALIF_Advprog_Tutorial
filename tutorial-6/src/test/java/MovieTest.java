import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.Before;
import java.util.Objects;

public class MovieTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    private Movie movie;
    private Movie movie2;

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Tebaatusasula", Movie.REGULAR);
    }


    @Test
    public void getTitle() {

        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {

        movie.setTitle("Bad Black");

        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {

        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {

        movie.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void equalsNull(){
        assertFalse(movie.equals(null));
    }

    @Test
    public void equalsWithItSelf(){
        assertTrue(movie.equals(movie));
    }

    @Test
    public void equalsWithAnotherMovie(){
        assertFalse(movie.equals(movie2));
    }

    @Test
    public void testHash(){
        assertEquals(Objects.hash(movie.getTitle(), movie.getPriceCode()), movie.hashCode());
    }
}