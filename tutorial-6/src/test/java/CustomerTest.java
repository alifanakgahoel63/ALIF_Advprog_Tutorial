import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.Before;


public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    private Customer customer;
    private Movie movie;
    private Movie movie2;
    private Movie movie3;
    private Rental rent;
    private Rental rent2;
    private Rental rent3;

    @Before
    public void setUp(){
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
        movie2 = new Movie("Tebaatusasula", Movie.NEW_RELEASE);
        rent2 = new Rental(movie2, 2);
        movie3 = new Movie("Bad Black", Movie.CHILDREN);
        rent3 = new Rental(movie3, 10);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    public void statementWithMultipleMovies() {
        // TODO Implement me!
        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertTrue(result.contains("4 frequent renter points"));
        assertTrue(result.contains("Amount owed is 21.5"));
        assertEquals(6, lines.length);
    }

    @Test
    public void htmlStatementTestWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertTrue(result.contains("Amount owed is 3.5<br>"));
        assertTrue(result.contains("1 frequent renter points"));
        assertEquals(4, lines.length);
    }

    @Test
    public void htmlStatementTestWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertEquals(6, lines.length);
        assertTrue(result.contains("4 frequent renter points"));
        assertTrue(result.contains("Amount owed is 21.5<br>"));
    }

}