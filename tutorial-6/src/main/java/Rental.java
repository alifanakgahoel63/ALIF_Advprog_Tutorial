class Rental {

    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public double calculateAmount() {
        double thisAmount = 0;

        switch (this.movie.getPriceCode()) {
            case Movie.REGULAR:
                thisAmount += 2;
                if (this.daysRented > 2) {
                    thisAmount += (this.daysRented - 2) * 1.5;
                }
                break;
            case Movie.NEW_RELEASE:
                thisAmount += this.daysRented * 3;
                break;
            case Movie.CHILDREN:
                thisAmount += 1.5;
                if (this.daysRented > 3) {
                    thisAmount += (this.daysRented - 3) * 1.5;
                }
                break;
            default:
        }

        return thisAmount;
    }

}