package tutorial.javari;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

import org.springframework.web.bind.annotation.*;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import static org.apache.logging.log4j.message.MapMessage.MapFormat.JSON;

@RestController
@RequestMapping(value = "/javari")
public class JavariController {
    // TODO Implement me!

    Path path = Paths.get("animals_records.csv");

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Animal> getListOfAnimal(){
        List<Animal> listOfAnimal = new ArrayList<>();
        try(BufferedReader reader = Files.newBufferedReader(path)) {
            String line = reader.readLine();
            while (line != null){
                String[] information = line.split(",");
                int id = Integer.parseInt(information[0]);
                String type = information[1];
                String name = information[2];
                Gender gender = Gender.parseGender(information[3]);
                double length = Double.parseDouble(information[4]);
                double weight = Double.parseDouble(information[5]);
                System.out.println(information[7]);
                Condition condition = Condition.parseCondition(information[7]);
                Animal animal = new Animal(id,type,name,gender,length,weight,condition);
                listOfAnimal.add(animal);
                line = reader.readLine();
            }
        }
        catch (IOException io){
            //do nothing
        }
        if(listOfAnimal.isEmpty()){
            throw new NullPointerException("Animal nya ga ada");
        }
        return listOfAnimal;
    }

    @RequestMapping(value = "/{ID}", method = RequestMethod.GET)
    public Animal getIndividualInformation(@PathVariable String ID){
        try(BufferedReader reader = Files.newBufferedReader(path)) {
            String line = reader.readLine();
            while (line != null){
                String[] information = line.split(",");
                int id = Integer.parseInt(information[0]);
                if (id == Integer.parseInt(ID)){
                    String type = information[1];
                    String name = information[2];
                    Gender gender = Gender.parseGender(information[3]);
                    double length = Double.parseDouble(information[4]);
                    double weight = Double.parseDouble(information[5]);
                    Condition condition = Condition.parseCondition(information[7]);
                    Animal animal = new Animal(id,type,name,gender,length,weight,condition);
                    return animal;
                }
                line = reader.readLine();
            }
        }
        catch (IOException io){
            //do nothing
        }
        throw new NullPointerException("ID nya ga ada");
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public Animal deleteAnAnimal(@PathVariable String id){
        List<Animal> listOfAnimal = getListOfAnimal();
        Animal copyAnimal = null;
        for (int i = 0; i < listOfAnimal.size(); i++){
            if (listOfAnimal.get(i).getId().equals(Integer.parseInt(id))){
                copyAnimal = listOfAnimal.get(i);
                listOfAnimal.remove(i);
            }
        }
        System.out.println(listOfAnimal.size());
        if (copyAnimal == null) throw new NullPointerException("ID yang mau dihapus ga ada");
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < listOfAnimal.size(); i++){
                Animal animal = listOfAnimal.get(i);
                String condition = (animal.getCondition().toString().equalsIgnoreCase("healthy")) ? "healthy" : "not healthy";
                sb.append(animal.getId()+","+animal.getType()+","+animal.getName()+","+animal.getGender().toString()+","+animal.getLength()+","+animal.getWeight()+","+""+","+condition+"\n");
            }
            BufferedWriter bw = new BufferedWriter(new FileWriter("animals_records.csv"));
            bw.write(sb.toString());
            bw.close();
        }
        catch (IOException io){
            //do nothing
        }
        return copyAnimal;
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addAnAnimal(@RequestParam(value = "identity", required = true) String identity){
       String[] temp = identity.split(",");
       for (int i = 0; i < temp.length; i++ ){
           System.out.println(temp[i]);
       }
       String[] information = new String[8];
       System.out.println(temp.length);
       for (int i = 0; i < temp.length; i++){
           if(i == 0) information[i] = temp[i].split("[:{}]")[2];
           else information[i] = temp[i].split("[:{}]")[1];
           System.out.println(information[i]);
       }
        /*int id = Integer.parseInt(information[0]);
        String type = information[1];
        String name = information[2];
        Gender gender = Gender.parseGender(information[6]);
        double length = Double.parseDouble(information[4]);
        double weight = Double.parseDouble(information[7]);
        Condition condition = Condition.parseCondition(information[3]);
        Animal animal = new Animal(id,type,name,gender,length,weight,condition);
        return animal;*/
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("animals_records.csv",true));
            bw.append(information[0]+","+information[1]+","+information[2]+","+information[6]+","+information[4]+","+information[7]+","+""+","+information[3]+"\n");
            bw.close();
        }
        catch (IOException io){
            //do nothing
        }
    }
}
