package applicant;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.function.Predicate;
import static org.junit.Assert.*;
public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!
    private static Applicant applicant;
    private static Predicate<Applicant> creditCheck;
    private static Predicate<Applicant> employmentCheck;
    private static Predicate<Applicant> crimeCheck;
    private static Predicate<Applicant> qualifiedCheck;

    @Before
    public void setUp() throws Exception{
        applicant = new Applicant();
        creditCheck = theApplicant -> theApplicant.getCreditScore() > 600;
        employmentCheck = theApplicant -> theApplicant.getEmploymentYears() > 0;
        crimeCheck = theApplicant -> !theApplicant.hasCriminalRecord();
        qualifiedCheck = theApplicant -> theApplicant.isCredible();
    }

    @Test
    public void testEvaluateMethodIfQualified(){
        assertEquals(true,Applicant.evaluate(applicant,employmentCheck.and(creditCheck).and(qualifiedCheck)));
    }

    @Test
    public void testEvaluateMethodIfNotQualified(){
        assertEquals(false,Applicant.evaluate(applicant,employmentCheck.and(creditCheck).and(crimeCheck).and(qualifiedCheck)));
    }

    @Test
    public void testPrintEvaluationMethodIfQualified(){
        boolean result = Applicant.evaluate(applicant,employmentCheck.and(creditCheck).and(qualifiedCheck));
        String msg = "Result of evaluating applicant: %s";
        msg = result ? String.format(msg, "accepted") : String.format(msg, "rejected");
        String expected = "Result of evaluating applicant: accepted";
        assertEquals(expected,msg);
    }

    @Test
    public void testPrintEvaluationMethodIfNotQualified(){
        boolean result = Applicant.evaluate(applicant,employmentCheck.and(creditCheck).and(crimeCheck).and(qualifiedCheck));
        String msg = "Result of evaluating applicant: %s";
        msg = result ? String.format(msg, "accepted") : String.format(msg, "rejected");
        String expected = "Result of evaluating applicant: rejected";
        assertEquals(expected,msg);
    }

    @Test
    public void testIsCredible(){
        assertEquals(true,applicant.isCredible());
    }

    @Test
    public void testGetCreditScore(){
        assertEquals(700,applicant.getCreditScore());
    }

    @Test
    public void testGetEmploymentYears(){
        assertEquals(10,applicant.getEmploymentYears());
    }

    @Test
    public void testHasCriminalRecord(){
        assertEquals(true,applicant.hasCriminalRecord());
    }
}