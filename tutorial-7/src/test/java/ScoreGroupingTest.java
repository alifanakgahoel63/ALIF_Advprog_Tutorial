import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sun.reflect.annotation.ExceptionProxy;

public class ScoreGroupingTest {
    // TODO Implement me!
    private static Map<String,Integer> scores = new HashMap<>();
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!
    @Before
    public void setUp() throws Exception{
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
    }

    @Test
    public void testWhetherGroupingResultIsRight() throws Exception{
        Map<Integer,List<String>> groupByScore = new HashMap<>();
        for(int i = 11; i <= 15; i++){
            if(i == 13 || i == 14) continue;
            List<String> peopleInGroup = new ArrayList<>();
            groupByScore.put(i,peopleInGroup);
        }
        groupByScore.get(11).add("Charlie");
        groupByScore.get(11).add("Foxtrot");
        groupByScore.get(12).add("Alice");
        groupByScore.get(15).add("Emi");
        groupByScore.get(15).add("Bob");
        groupByScore.get(15).add("Delta");
        assertEquals(groupByScore,ScoreGrouping.groupByScores(scores));
    }
}